# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.5.2] - Alien Abduction Reloaded - 2019-03-10

### Added

- Enable to properly replace string logs with % operator
- Added Gitlab CI building and deploying